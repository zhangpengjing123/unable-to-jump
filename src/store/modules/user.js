import { login, getUserInfo } from '@/api/user'
// import { Message } from 'element-ui'
import { setToken, getToken } from '@/utils/auth'
const state = {
  // 储存 token
  token: getToken(),
  userInfo: {}
}
const mutations = {
  // 修改token
  setToken(state, data) {
    state.token = data
    // 除了将 token 放入 state ，还需要数据持久化
    setToken(data)
  },
  // 设置用户数据
  setUserInfo(state, data) {
    // 为了避免数据的污染，最好拷贝一份新的对象
    state.userInfo = { ...data }
  },
  removeUserInfo(state) {
    state.userInfo = {}
  },
  removeToken(state) {
    state.token = ''
  }

}
const actions = {
  // 这里应该是被页面调用的 actions
  // 页面调用时应该传入 表单
  // 1.发请求登录
  async login(store, data) {
    // console.log(res)
    const res = await login(data)
    // 2.利用 mutations 存储token
    // 因为在响应拦截器已经将 res.data.data 解构
    // 这里面的 res 就是直接我们需要的数据
    // store.commit('setToken', res.data.data)
    store.commit('setToken', res)
    // console.log('成功')
    // login(data).then(res => {
    //   console.log(res)
    //   // 2.利用 mutations 存储token
    //   store.commit('setToken', res.data.data)
    // })
  },
  async getUserInfo(store) {
    const res = await getUserInfo()
    // console.log('拿到了：', res)
    store.commit('setUserInfo', res)
  }
}

export default {
  // 命名锁
  namespaced: true,
  state,
  mutations,
  actions
}
