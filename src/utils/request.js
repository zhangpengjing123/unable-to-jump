// 引入库
import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
// create an axios instance
// 创建实例
const service = axios.create({
//  正式请求地址 = 基准路径 + 请求api封装路径
  baseURL: process.env.VUE_APP_BASE_API,
  // 如果 5000 毫秒都没有响应，就算超时
  timeout: 5000 // request timeout
})

// 请求拦截器
// 这里跟响应拦截器一样
// 可以接受两个参数
// 1.成功请求情况的回调
// 2.失败请求的回调（暂时没有失败情况，先写一个占位）
// request interceptor
service.interceptors.request.use(
  // 成功请求情况的回调（可以拦截到当前请求的配置 一般形参叫 config）
  config => {
    // 前提条件：有tokan
    if (store.getters.tokan) {
      // 这里我们希望统一添加 tokan
      // 我们这个项目使用的是 Bearer JWT token 标准
      // 固定标准写法为 'Bearer xxx'
      config.headers.Authorization = `Bearer ${store.getters.tokan}`
    }
    // 凡是有拦截，请记得放行
    return config
  },
  // 2.失败请求的回调（暂时没有失败情况，先写一个占位）
  err => {
    console.log(err)
    return Promise.reject('请求失败')
  }
)

// 响应拦截器
// 响应拦截器可以接受两个参数
// 1.网络层面成功的回调
// 2.网络层面失败的回调
// response interceptor
service.interceptors.response.use(
  res => { // 硬性成功下，数据成功
    // 这是网络层面的成功
    console.log('网络层面的成功')
    console.log(res)
    const { success, message, data } = res.data
    if (success) {
      // 返回数据
      // 这里时候网络成功，数据也成功
      // 记住一旦拦截了就必须放行不然没数据
      return data
    } else { // 硬性成功下数据失败
      // 数据层面的失败
      // 1.提示错误
      Message.error(message)
      // 2.拒绝当前的 promise 继续执行
      return Promise.reject(new Error(message))
    }
  },
  err => { // 硬性失败
    console.log(err)
    // 网络层面的失败   响应拦截器硬性网络请求错误的方法
    // 1.提示错误
    Message.error('系统繁忙，请稍后再试')
    // 2.拒绝当前的 promise 继续执行
    // 默认情况下，请求的 promise 都是走成功的路径
    // 如果想在报错的时候，手动强制停止当前 promise 请求
    return Promise.reject(new Error('系统繁忙，请稍后再试'))
  }
)

export default service
