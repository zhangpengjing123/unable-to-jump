// 引入路由实例
import router from '@/router'
// 引入 store vuex 实例
import store from '@/store'

// 调用路由守卫
router.beforeEach(async(to, from, next) => {
  // to 是目的地，from 是来源 ，next 是放行
  const token = store.getters.tokan
  const url = to.path
  //   白名单
  const whiteList = ['/login', '/404']
  // 1.有 tokan 去登录页 => 跳到首页
  if (token && url === '/login') {
    next('/')
  }

  // 2.有 tokan 不是去登录页 => 放行
  if (token && url !== '/login') {
    // 先拿到数据在放行
    next()
  }

  // 3.没有 tokan 在白名单里 => 放行
  if (!token && whiteList.includes(url)) {
    next()
  }

  // 4.没有 tokan 不在白名单里 => 跳到登录页
  if (!token && !whiteList.includes(url)) {
    next('/login')
  }
})
